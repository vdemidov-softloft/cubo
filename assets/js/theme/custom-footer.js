export default function () {
    const dataFoot = [];
    let buttonList;
    const lines = document.querySelectorAll('.footer-info-col');
    lines.forEach(item => {
        if (item.dataset.sectionType !== 'newsletterSubscription') {
            const child = item.children;
            let head = '';
            let part = '';
            for (let i = 0; i < child.length; i++) {
                if (i === 0) {
                    head = child[i].outerHTML;
                } else {
                    part += child[i].outerHTML;
                }
            }
            const obj = {
                header: head,
                body: part,
            };
            dataFoot.push(obj);
        }
    });
    const chengerMob = (parts) => {
        lines.forEach((e, index) => {
            if (e.dataset.sectionType !== 'newsletterSubscription') {
                const divBloc = document.createElement('div');
                divBloc.classList.add('expand-wraper');
                divBloc.innerHTML = `
                    <div class='see-you'>${parts[index].header} <span class="show-marker"></span></div>
                    <div class='expand'>${parts[index].body}</div>
                `;
                e.innerHTML = '';
                e.appendChild(divBloc);
                buttonList = document.querySelectorAll('.see-you');
            }
        });
    };
    const chengerTab = () => {
        lines.forEach((e, index) => {
            if (e.dataset.sectionType === 'newsletterSubscription') {
                e.classList.add('asider');
            } else {
                e.classList.add(`blocker-${index}`);
            }
        });
    };
    if (window.innerWidth < 480) {
        chengerMob(dataFoot);
        buttonList.forEach(but => {
            // eslint-disable-next-line no-param-reassign
            but.onclick = () => {
                const marker = but.querySelector('.show-marker');
                const show = document.querySelectorAll('.show__this');
                const hide = document.querySelectorAll('.hide-marker');
                if (!but.nextElementSibling.classList.contains('show__this')) {
                    show.forEach(elem => {
                        elem.classList.remove('show__this');
                    });
                    hide.forEach(elem => {
                        elem.classList.remove('hide-marker');
                    });
                }
                marker.classList.toggle('hide-marker');
                but.nextElementSibling.classList.toggle('show__this');
            };
        });
    }
    if (window.innerWidth < 769) {
        chengerTab();
    }
}
